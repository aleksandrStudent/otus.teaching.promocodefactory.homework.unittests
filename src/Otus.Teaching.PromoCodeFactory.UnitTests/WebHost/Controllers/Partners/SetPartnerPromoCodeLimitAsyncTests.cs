﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Linq;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;
        private readonly IFixture _fixture;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            _fixture = new Fixture().Customize(new AutoMoqCustomization());
            _fixture.Behaviors.OfType<ThrowingRecursionBehavior>().ToList()
                .ForEach(b => _fixture.Behaviors.Remove(b));
            _fixture.Behaviors.Add(new OmitOnRecursionBehavior());
            _partnersRepositoryMock = _fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = _fixture.Build<PartnersController>().OmitAutoProperties().Create();

        }

        /// <summary>
        /// Если партнер не найден, то также нужно выдать ошибку 404;
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            // Arrange
            var partnerId = Guid.Parse("7d994823-8226-4273-1111-1a95f3cc1df8");
            Partner partner = null;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            //var request = CreateSetPartnerPromoCodeLimitRequest();
            var request = _fixture.Create<SetPartnerPromoCodeLimitRequest>();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        /// <summary>
        /// Если партнер заблокирован, то есть поле IsActive=false в классе Partner, то также нужно выдать ошибку 400;
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsActiveFalse_ReturnsBadRequest()
        {
            // Arrange
            var partner = _fixture.Build<Partner>().With(p => p.IsActive, false).Create();

            var request = _fixture.Create<SetPartnerPromoCodeLimitRequest>();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();

        }

        /// <summary>
        /// Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, 
        /// которые партнер выдал NumberIssuedPromoCodes, если лимит закончился, то количество не обнуляется;
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetNewLimitAndNumberIssuedPromoCodes_ReturnsZero()
        {
            // Arrange
            _fixture.Customizations.Insert(0, new RandomNumericSequenceGenerator(1, int.MaxValue));

            var partner = _fixture.Create<Partner>();
            var request = _fixture.Create<SetPartnerPromoCodeLimitRequest>();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();

        }

        /// <summary>
        /// При установке лимита нужно отключить предыдущий лимит;
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetMewLimitDisableOldLimit_ReturnsNull()
        {
            // Arrange

            var partner = _fixture.Create<Partner>();
            var request = _fixture.Create<SetPartnerPromoCodeLimitRequest>();

            var expectedNumberIssuedPromocodes = partner.NumberIssuedPromoCodes;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
            partner.NumberIssuedPromoCodes.Should().Be(expectedNumberIssuedPromocodes);

        }

        /// <summary>
        /// Лимит должен быть больше 0;
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_LimitMoreThenZero_ReturnsBadRequest()
        {
            // Arrange
            var partner = _fixture.Create<Partner>();
            var request = _fixture.Create<SetPartnerPromoCodeLimitRequest>();
            
            request.Limit = 0;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
               .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        /// <summary>
        /// Нужно убедиться, что сохранили новый лимит в базу данных (это нужно проверить Unit-тестом);
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_NewLimitSaveToDB_ReturnsNewLimit()
        {
            // Arrange
            var partner = _fixture.Create<Partner>();
            var request = _fixture.Create<SetPartnerPromoCodeLimitRequest>();
            
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            partner.PartnerLimits.Should().Contain(x => x.Limit == request.Limit);
            _partnersRepositoryMock.Verify(x => x.UpdateAsync(partner));
        }

      
    }
}